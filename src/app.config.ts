import { resolve } from 'path';

const appConfig = {
  isProduction: process.env.NODE_ENV === 'production',
  mongoURI: process.env.MONGO_URI,
  mockData: !!parseInt(process.env.MOCK_DATA),
  apiPath: process.env.API_PATH,
  filePath: resolve('files'),
  fileHost: process.env.FILE_HOST,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpire: process.env.JWT_EXPIRE,
  jwtIssuer: process.env.JWT_ISSUER,
  jwtRefreshExpire: process.env.JWT_REFRESH_EXPIRE,
  jwtRefreshSecret: process.env.JWT_REFRESH_SECRET,
};

export default appConfig;
