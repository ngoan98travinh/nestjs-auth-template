import { Field, ObjectType } from '@nestjs/graphql';
import { Prop, Schema } from '@nestjs/mongoose';

@Schema({ timestamps: true })
@ObjectType({ isAbstract: true })
export class BaseObject {
  @Field()
  _id: string;

  @Field()
  @Prop({ default: Date.now })
  createdAt: Date;

  @Field()
  @Prop({ default: null })
  createdBy: string;

  @Field({ nullable: true })
  @Prop({ default: null })
  updatedAt?: Date;

  @Field({ nullable: true })
  @Prop({ default: null })
  updatedBy: string;

  @Prop({ default: null })
  deletedAt: Date;

  @Prop({ default: null })
  deletedBy: string;

  @Prop({ default: false })
  isDeleted: boolean;
}
