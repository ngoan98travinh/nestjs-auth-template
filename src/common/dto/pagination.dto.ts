import { InputType, Field, Int } from '@nestjs/graphql';

@InputType({ isAbstract: true })
export class PaginationInput {
  @Field(() => Int)
  limit: number;

  @Field(() => Int)
  offset: number;
}
