import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { LoginInput } from './dto/login.input';
import { Token } from './entities/token.entity';
import { CurrentUser } from './gql-user.decor';
import { Public } from './public.decor';

@Resolver()
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) {}

  @Public()
  @Mutation(() => Token)
  async login(@Args('loginInput') loginInput: LoginInput) {
    const user = await this.authService.validateUser(
      loginInput.username,
      loginInput.password,
    );
    return this.authService.login(user);
  }

  @Public()
  @Mutation(() => Token)
  async refresh(
    @Args('userId') userId: string,
    @Args('refreshToken') refreshToken: string,
  ) {
    return this.authService.refresh(userId, refreshToken);
  }

  @Query(() => User)
  async currentUser(@CurrentUser() user: any) {
    return this.userService.findOne(user.userId);
  }
}
