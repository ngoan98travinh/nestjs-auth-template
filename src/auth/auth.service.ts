import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import { compare } from 'bcrypt';
import appConfig from 'src/app.config';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { Token } from './entities/token.entity';

type TokenPayload = { username: string; sub: string };

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<User> {
    const user = await this.usersService.findOne(username, 'username');
    if (user) {
      const matched = await compare(pass, user.password);
      if (matched) {
        const { password, ...result } = user.toObject();
        return result;
      }
    }
    throw new UserInputError('Username or password is incorrect.');
  }

  async login(user: User): Promise<Token> {
    const payload: TokenPayload = { username: user.username, sub: user._id };
    return {
      accessToken: this.jwtService.sign(payload),
      refreshToken: this.jwtService.sign(payload, {
        expiresIn: appConfig.jwtRefreshExpire,
        secret: appConfig.jwtRefreshSecret,
      }),
    };
  }

  async refresh(userId: string, refreshToken: string): Promise<Token> {
    try {
      const payload = this.jwtService.verify<TokenPayload>(refreshToken, {
        secret: appConfig.jwtRefreshSecret,
      });
      if (payload.sub === userId) {
        return {
          accessToken: this.jwtService.sign({
            uername: payload.username,
            sub: payload.sub,
          }),
          refreshToken,
        };
      }
      throw new AuthenticationError('Token mismatch');
    } catch (error) {
      this.logger.error(error.message, error.stack);
      throw new AuthenticationError(error.message);
    }
  }
}
