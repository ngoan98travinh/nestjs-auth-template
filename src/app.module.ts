import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import appConfig from './app.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import mongoose from 'mongoose';
import { SoftDelete } from 'soft-delete-mongoose-plugin';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { GqlAuthGuard } from './auth/gql-auth.guard';

@Module({
  imports: [
    MongooseModule.forRoot(appConfig.mongoURI),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      debug: !appConfig.isProduction,
      playground: !appConfig.isProduction,
      autoSchemaFile: true,
      mocks: appConfig.mockData,
      path: appConfig.apiPath,
      cors: {
        origin: '*',
        credentials: true,
      },
      definitions: {
        emitTypenameField: true,
      },
    }),
    UsersModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: GqlAuthGuard,
    },
  ],
})
export class AppModule {
  constructor() {
    // eslint-disable-next-line
    mongoose.plugin(require('mongoose-nanoid'));
    mongoose.plugin(
      new SoftDelete({
        isDeletedField: 'isDeleted',
        deletedAtField: 'deletedAt',
      }).getPlugin(),
    );
    console.log({ appConfig });
  }
}
