import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { User, UserDocument } from './entities/user.entity';
import { UserInputError } from 'apollo-server-express';
import { SoftDeleteModel } from 'soft-delete-mongoose-plugin';
import { QueryUserInput } from './dto/query-user.input';
import { hash } from 'bcrypt';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  constructor(
    @InjectModel(User.name)
    private readonly userModel: SoftDeleteModel<UserDocument>,
  ) {}

  async create(createUserInput: CreateUserInput) {
    try {
      createUserInput.password = await hash(createUserInput.password, 10);
      const createdUser = await this.userModel.create(createUserInput);
      this.logger.log(`Added a new user at id#${createdUser?._id}`);
      return createdUser;
    } catch (error) {
      this.logger.error(error.message, error.stack);
      if (error.message?.includes('E11000 duplicate key')) {
        throw new UserInputError('Duplicated username');
      }
      throw new Error(error);
    }
  }

  findAll(queryUserInput: QueryUserInput) {
    const { offset, limit, ...filter } = queryUserInput;
    return this.userModel.find(filter).skip(offset).limit(limit);
  }

  findOne(id: string, customField?: string) {
    if (customField) {
      return this.userModel.findOne({
        [customField]: id,
      });
    }
    return this.userModel.findById(id);
  }

  async update(id: string, updateUserInput: UpdateUserInput) {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      id,
      {
        $set: updateUserInput,
      },
      { new: true },
    );
    this.logger.log(`Updated a user at id#${updatedUser?._id}`);
    return updatedUser;
  }

  async remove(id: string) {
    const removedUser = await this.userModel.findByIdAndSoftDelete(id);
    if (!removedUser) {
      throw new UserInputError('No user found by given id');
    }
    this.logger.log(`Removed a user at id#${removedUser?._id}`);
    return removedUser;
  }
}
