import { ObjectType, Field, registerEnumType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { BaseObject } from 'src/common/entities/base-object.entity';

export enum UserRole {
  Administrator = 'Administrator',
  Manager = 'Manager',
  User = 'User',
}

registerEnumType(UserRole, {
  name: 'UserRole',
});

export enum UserStatus {
  Active = 'Active',
  Pending = 'Pending',
  Inactive = 'Inactive',
}

registerEnumType(UserStatus, {
  name: 'UserStatus',
});

export type UserDocument = User & Document;

@Schema()
@ObjectType()
export class User extends BaseObject {
  @Field()
  @Prop({
    unique: true,
    trim: true,
    index: true,
  })
  username: string;

  @Field({ nullable: true })
  @Prop({
    unique: true,
    sparse: true,
    trim: true,
    index: true,
  })
  tel?: string;

  @Field({ nullable: true })
  @Prop({
    unique: true,
    sparse: true,
    trim: true,
    index: true,
  })
  email?: string;

  @Prop({
    required: true,
  })
  password?: string;

  @Field({ nullable: true })
  @Prop()
  displayName: string;

  @Field({ nullable: true })
  @Prop()
  avatar?: string;

  @Field(() => UserRole)
  @Prop({ required: true })
  role: UserRole;

  @Field(() => UserStatus)
  @Prop({ required: true })
  status: UserStatus;
}

export const UserSchema = SchemaFactory.createForClass(User);
