import { Field, InputType } from '@nestjs/graphql';
import { PaginationInput } from 'src/common/dto/pagination.dto';
import { UserRole, UserStatus } from '../entities/user.entity';

@InputType()
export class QueryUserInput extends PaginationInput {
  @Field(() => UserRole, { nullable: true })
  roles?: UserRole;

  @Field(() => UserStatus, { nullable: true })
  status?: UserStatus;
}
