db.users.insertOne({
  _id: '000000000000',
  createdBy: null,
  updatedAt: null,
  updatedBy: null,
  deletedAt: null,
  deletedBy: null,
  isDeleted: false,
  username: 'superuser',
  password: '$2b$10$6v1InlyVFAVU2NkiIbe07O5.9r4d/LYUtqIUwsgdfEV9I44gpeF32', // supersecret
  roles: ['Administrator'],
  status: 'Active',
  createdAt: ISODate('2022-04-14T03:35:57.725Z'),
  email: 'admin@mail.local',
  title: 'Administrator',
  displayName: 'Administrator',
});
